//starting requirement for known files and commands
var data         = require('../db.js');
var api          = require('../api.js');
var host         = require('../respond.js');

//starting requirement for services and processes
var HTTPS        = require('https');
var request      = require('request');
const mongodb    = require('mongodb');

async function getOdds() {
    oddsTerm = host.vexTerm.split(" ");
    let team = '1961X';
    let matches = await data.db.collection('matches').find({
        $and: [{
            season: 119
        }, {
            $or: [{
                red: team
            }, {
                red2: team
            }, {
                red3: team
            }, {
                blue: team
            }, {
                blue2: team
            }, {
                blue3: team
            }]
        }]
    }).toArray();

    var teamArr = [];
    teamArr[0] = matches[oddsTerm[0] - 1].red;
    teamArr[1] = matches[oddsTerm[0] - 1].red2;
    teamArr[2] = matches[oddsTerm[0] - 1].blue;
    teamArr[3] = matches[oddsTerm[0] - 1].blue2;

    var whoami = 0;
    if (teamArr[0] == '1961X') {
        whoami = 1;
    } else if (teamArr[1] == '1961X') {
        whoami = 1;
    }

    var tElo = [];

    for (var i = 0; i < teamArr.length; i++) {
        var eloCol = await data.db.collection('elo').findOne({
            _id: {
                id: teamArr[i],
                prog: 1
            }
        });
        tElo[i] = eloCol.elo;
    }

    var aElo = [];

    //red alliance
    aElo[0] = tElo[0] + tElo[1];

    //blue alliance
    aElo[1] = tElo[2] + tElo[3];

    console.log(aElo[0] + " and " + aElo[1]);

    //red alliance chances
    aChances[0] = aElo[0] / (aElo[0] + aElo[1]);
    //blue alliance chances
    aChances[1] = aElo[1] / (aElo[0] + aElo[1]);

    if (oddsTerm[0] > matches.length) {
        aChances[0] = "n/a";
        aChances[1] = "n/a";
    }

    postMessage("Match " + oddsTerm[0] + ": Red Alliance (" + teamArr[0] + ", " + teamArr[1] + ") has a " + Math.round(aChances[0] * 100) + " percent chance and blue alliance (" + teamArr[2] + ", " + teamArr[3] + ") has a " + Math.round(aChances[1] * 100) + " percent chance.");
}

exports.getOdds = getOdds;

/*


        vexTerm = request.text.substr(6);
        oddsTerm = vexTerm.split(" ");
        this.res.writeHead(200);
        getOdds(vexTerm);
        this.res.end();

        
*/