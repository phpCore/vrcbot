//starting requirement for known files and commands
    var data         = require('../db.js');
    var api          = require('../api.js');
    var host         = require('../respond.js');

//starting requirement for services and processes
    var HTTPS        = require('https');
    var request      = require('request');
    const mongodb    = require('mongodb');

var exports = module.exports = {};

function sleep (ms) { // number of milliseconds to sleep
    return new Promise(function (resolve) { // creates a new promise
        setTimeout(resolve, ms); // promise will be resolved when the timeout completes
    });
}
    
exports.checkMatch = async function (vexTerm) {

    vexAlloc = vexTerm.split(" ");

    let team = '1961X';
    let matches = await data.db.collection('matches').find({
        $and: [{
            season: 119
        }, {
            $or: [{
                red: team
            }, {
                red2: team
            }, {
                red3: team
            }, {
                blue: team
            }, {
                blue2: team
            }, {
                blue3: team
            }]
        }]
    }).toArray();

            if(vexAlloc[0] > 0) {
                var final = vexAlloc[0];
            }
            else {
                api.postMessage("Please select a competition first:" + "\r\n" + "(1): " + host.compname[1]  + "\r\n" + "(2): " + host.compname[2] + "\r\n" + "(3): " + host.compname[3] + "\r\n" + "(4): " + host.compname[4] + "\r\n" + "(5): " + host.compname[5]);
                await sleep(5000);
                var request  = host.request;
                var event_selected = host.comp[request.text];
                console.log(event_selected);

                let event = event_selected;
                let events = await data.db.collection('events').find({ $and: [{ season: 119 }, { $or: [{ _id: event }] }] }).toArray();

                var myEvent = [];
                var n = 0;
                for(i = 0; i < matches.length; i++) {
                    if(matches[i]._id.event == event_selected) {
                        myEvent[n] = i;
                        n++;
                    }
                }


                console.log(myEvent.length);
                console.log(myEvent);

                var matchType = [];
                var matchText = [];
                for (i = 0; i < myEvent.length; i++) {
                 matchType[i] = matches[myEvent[i]]._id.round;
                     if(matchType[i] == 3) {
                         matchText[i] = "QF" + " " + matches[myEvent[i]]._id.instance + "-" + matches[myEvent[i]]._id.number;
                     } 
                     else if(matchType[i] == 4) {
                        matchText[i] = "SF" + " " + matches[myEvent[i]]._id.instance + "-" + matches[myEvent[i]]._id.number;
                    } 
                    else if(matchType[i] == 5) {
                        matchText[i] = "F" + " " + matches[myEvent[i]]._id.instance + "-" + matches[myEvent[i]]._id.number;
                    } 
                    else if(matchType[i] == 2) {
                        matchText[i] = "Q" + matches[myEvent[i]]._id.number;
                    }
                }

                var qualData = "";
                for(i = 0; i < myEvent.length; i++) {
                    n = i + 1;
                        qualData = qualData + "\r\n" + "(" + n + "): " + matchText[i];  
                }

                request = "";
                api.postMessage("Pick one! In that competition, you were in " + myEvent.length + " matches." + qualData);
                await sleep(5000);
                request = host.request;
                
                api.postMessage(matchText[request.text-1] + " @ " + events[0].name + "\r\n" + "🔵: " + matches[myEvent[request.text-1]].blue + " and " + matches[myEvent[request.text-1]].blue2 + "\r\n" + "Score: " + matches[myEvent[request.text-1]].blueScore + "\r\n" + "🔴: " + matches[myEvent[request.text-1]].red + " and " + matches[myEvent[request.text-1]].red2 + "\r\n" + "Score: " + matches[myEvent[request.text-1]].redScore);
            }


    var redteam = matches[final - 1].red;
    var red2team = matches[final - 1].red2;
    var blueteam = matches[final - 1].blue;
    var blue2team = matches[final - 1].blue2;
    var scorered = matches[final - 1].redScore;
    var scoreblue = matches[final - 1].blueScore;

    var roundnum = matches[final]._id.round;

    var eventid = matches[final]._id.event;

    let event = eventid;
    let events = await data.db.collection('events').find({ $and: [{ season: 119 }, { $or: [{ _id: event }] }] }).toArray();

    var roundtype;
    if (roundnum == 1) {
        roundtype = "P";
    }
    if(roundnum == 2) {
        roundtype = "Q";
    }
    else if(roundnum == 3) {
        roundtype = "QF";
    }
    else if(roundnum == 4) {
        roundtype = "SF";
    }
    else if(roundnum == 5) {
        roundtype = "F";
    }
    
    var instancenum = matches[final]._id.instance;

    var numbernum = matches[final]._id.number;

    var elim;
    if(roundnum > 2) {
        elim = roundtype + " " + instancenum + "-" + numbernum;
    }
    else {
        elim = roundtype + "" + numbernum;
    }


    var info = [redteam, red2team, blueteam, blue2team, scorered, scoreblue];

    api.postMessage(elim + " @ " + events[0].name + "\r\n" + "🔵: " + info[2] + " and " + info[3] + "\r\n" + "Score: " + info[5] + "\r\n" + "🔴: " + info[0] + " and " + info[1] + "\r\n" + "Score: " + info[4]);
};

