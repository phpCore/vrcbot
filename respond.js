//starting requirement for known files and commands
    var data         = require('./db.js');
    var api          = require('./api.js');
    var checkMatches = require('./commands/checkMatches.js');
    var getNextComp  = require('./commands/getNextComp.js');
    var getOdds      = require('./commands/getOdds.js');

//starting requirement for services and processes
    var HTTPS        = require('https');
    var request      = require('request');
    const mongodb    = require('mongodb');
    var cron         = require('node-cron');
    var botID        = process.env.BOT_ID;

//starting initial array storage block
    var comp        = ["na", "RE-VRC-17-2523", "RE-VRC-17-3986", "RE-VRC-17-4076", "RE-VRC-17-2776", "RE-VRC-17-2597"]; 

    var compname    = ["na", "In the Zone @ SFHS", "Cross Keys VRC Tournament - In The Zone", "GA TSA Fall Leadership",
                       "In the Zone (The Wild Wild West Zone)", "In the Zone at North Forsyth"];
    var events      = []; //initializing an empty array for later use and storage
    var comp_status = []; //
    var elo         = []; //
    var aChances    = []; //see above
    var botResponse = "";
    var vexTerm     = "";

//usage for automatically scheduling match updates and db pulls

async function respond() {
    var request  = JSON.parse(this.req.chunks[0]),
    regexMatches = request.text.indexOf('.matches');
    regexNComp   = request.text.indexOf('.ncomp');
    regexOdds    = request.text.indexOf('.odds');

    exports.request = request;

    if (regexMatches != -1 && request.name != 'vrcbot') {
        this.res.writeHead(200);
        vexTerm = request.text.substr(9);
        checkMatches.checkMatch(vexTerm);
        this.res.end();
    } 
    else { console.log("convo"); }
    
    if (regexNComp != -1) { 
        getNextComp.checkEvents();
    } 
    else { console.log("convo"); }

    if (regexOdds != -1) {
        vexTerm = request.text.substr(6);
        getOdds.getOdds;
    } 
    else { console.log("convo"); }

}

cron.schedule('*/1 * * * *', function() {
    getNextComp.checkEvents();    
});

exports.vexTerm = vexTerm;
exports.compname = compname;
exports.respond = respond;
exports.comp = comp;
exports.events = events;
exports.comp_status = comp_status;
exports.elo = elo;
exports.aChances = aChances;